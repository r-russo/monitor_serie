#include "monitor_serie.h"

#include "dialogs/dialog_serialport_setup.h"
#include "panels/panel_comandos.h"
#include "panels/panel_monitor.h"
#include "utils/events.h"

#include <sstream>
#include <wx/bmpbndl.h>
#include <wx/cmdproc.h>
#include <wx/datetime.h>
#include <wx/event.h>
#include <wx/gdicmn.h>
#include <wx/generic/splitter.h>
#include <wx/gtk/toolbar.h>
#include <wx/log.h>
#include <wx/splitter.h>
#include <wx/tbarbase.h>
#include <wx/timer.h>
#include <wx/toolbar.h>

MonitorSerie::MonitorSerie(const wxString &title)
    : wxFrame(nullptr, wxID_ANY, title, wxDefaultPosition, wxSize(800, 600)) {

    auto *toolbar = new wxToolBar(this, wxID_ANY, wxDefaultPosition,
                                  wxDefaultSize, wxTB_HORIZONTAL | wxTB_TEXT);
    toolbar->AddTool(ID_NEW, wxT("Nuevo"), wxBitmapBundle());
    toolbar->AddSeparator();
    toolbar->AddTool(ID_RUN, wxT("Iniciar"), wxBitmapBundle());
    toolbar->AddTool(ID_STOP, wxT("Parar"), wxBitmapBundle());
    toolbar->AddSeparator();
    toolbar->AddTool(ID_CONFIG, wxT("Configurar"), wxBitmapBundle());
    toolbar->AddSeparator();
    toolbar->AddCheckTool(ID_TOGGLE_MONITOR_SCROLL,
                          wxT("Bloquear desplazamiento"), wxBitmapBundle());
    toolbar->Realize();
    this->SetToolBar(toolbar);

    auto *splitter =
        new wxSplitterWindow(this, wxID_ANY, wxDefaultPosition, wxDefaultSize,
                             wxSP_BORDER | wxSP_LIVE_UPDATE);
    splitter->SetMinimumPaneSize(200);

    m_panel_comandos = new PanelComandos(splitter, ID_COMMANDS);
    m_panel_monitor = new PanelMonitor(splitter, ID_MONITOR);

    splitter->SplitVertically(m_panel_comandos, m_panel_monitor);

    GenerateMenubar();

    CreateStatusBar(2);
    SetStatusText(wxT("Test"));
    InitControlStates();
    BindEvents();
    this->Centre();
}

void MonitorSerie::OnSetup(__attribute__((unused)) wxCommandEvent &event) {
    DialogSerialPortSetup dialog(this);
    if (dialog.ShowModal() == wxID_OK) {
        wxLogDebug("Setting up serial port to device %s at %d baudrate",
                   dialog.GetPort(), dialog.GetBaudRate());
        m_serial_port.setPort(dialog.GetPort());
        m_serial_port.setBaudRate(dialog.GetBaudRate());
    }
}

void MonitorSerie::OnRun(__attribute__((unused)) wxCommandEvent &event) {
    if (m_serial_port.openPort()) {
        m_timer.Start(500);
    }
    UpdateControlStates();
}

void MonitorSerie::OnStop(__attribute__((unused)) wxCommandEvent &event) {
    m_serial_port.closePort();
    m_timer.Stop();
    UpdateControlStates();
}

void MonitorSerie::OnToggleMonitorScroll(wxCommandEvent &event) {
    m_panel_monitor->SetAutoscroll(event.IsChecked());
}

void MonitorSerie::OnTimer(__attribute__((unused)) wxTimerEvent &event) {
    // TODO: convertir en hilo
    std::string data_read{};

    while (1) {
        std::string buffer{};
        if (!m_serial_port.readBytes(buffer)) {
            wxCommandEvent event{};
            OnStop(event);
            return;
        }

        if (buffer.size() == 0) {
            break;
        }
        data_read += buffer;
    }

    // wxLogDebug("%s", buffer);
    if (data_read.size() > 0) {
        std::stringstream data_read_stream(data_read);

        for (std::string line; std::getline(data_read_stream, line);) {
            std::time_t now = std::time(nullptr);
            SerialData d = {now, data_read, false};
            m_serial_buffer.push_back(d);
            // FIXME: hacerlo configurable
            if (m_serial_buffer.size() > 1000) {
                m_serial_buffer.pop_front();
            }
            m_panel_monitor->AppendText(d);
        }
    }
}

void MonitorSerie::OnSendCommand(__attribute((unused)) wxCommandEvent &event) {
    std::string command = std::string(event.GetString());
    if (!m_serial_port.writeBuffer(command)) {
        wxLogDebug("Error: Buffer not sent");
        return;
    }
    std::time_t now = std::time(nullptr);
    SerialData d = {now, command, true};
    m_panel_monitor->AppendText(d);
}

void MonitorSerie::OnAddCommand(wxCommandEvent &event) {
    wxPostEvent(m_panel_comandos, event);
}

void MonitorSerie::GenerateMenubar() {
    wxMenuBar *menu = new wxMenuBar;
    wxMenu *menu_archivo = new wxMenu;
    menu_archivo->Append(ID_OPEN_COMMANDS, wxT("&Abrir comandos guardados"));
    menu_archivo->Append(ID_SAVE_COMMANDS, wxT("Guardar &comandos"));
    menu_archivo->Append(ID_SAVE_LOG, wxT("Guardar &log"));
    menu_archivo->Append(wxID_EXIT, wxT("&Salir"));
    wxMenu *menu_opciones = new wxMenu;
    menu_opciones->Append(wxID_ANY, wxT("&Configurar puerto serie"));
    menu_opciones->AppendCheckItem(wxID_ANY,
                                   wxT("&Interpretar comandos literales"));
    menu_opciones->AppendSeparator();
    menu_opciones->AppendRadioItem(wxID_ANY, wxT("&ASCII"));
    menu_opciones->AppendRadioItem(wxID_ANY, wxT("&HEX"));

    menu->Append(menu_archivo, wxT("&Archivo"));
    menu->Append(menu_opciones, wxT("&Opciones"));

    SetMenuBar(menu);
}

void MonitorSerie::InitControlStates() {
    GetToolBar()->ToggleTool(ID_TOGGLE_MONITOR_SCROLL, true);
    m_panel_monitor->SetAutoscroll(true);

    UpdateControlStates();
}

void MonitorSerie::BindEvents() {
    m_timer.Bind(wxEVT_TIMER, &MonitorSerie::OnTimer, this);
}

void MonitorSerie::UpdateControlStates() {
    auto toolbar = GetToolBar();
    toolbar->EnableTool(ID_RUN, !m_serial_port.isOpen());
    toolbar->EnableTool(ID_STOP, m_serial_port.isOpen());
    m_panel_monitor->Enable(m_serial_port.isOpen());
}

bool App::OnInit() {
    MonitorSerie *monitor_serie = new MonitorSerie(wxT("Monitor Serie"));
    monitor_serie->Show(true);

    return true;
}

BEGIN_EVENT_TABLE(MonitorSerie, wxFrame)
EVT_TOOL(ID_CONFIG, MonitorSerie::OnSetup)
EVT_TOOL(ID_RUN, MonitorSerie::OnRun)
EVT_TOOL(ID_STOP, MonitorSerie::OnStop)
EVT_TOOL(ID_TOGGLE_MONITOR_SCROLL, MonitorSerie::OnToggleMonitorScroll)
EVT_COMMAND(wxID_ANY, MONITOR_SEND_COMMAND, MonitorSerie::OnSendCommand)
EVT_COMMAND(wxID_ANY, MONITOR_ADD_COMMAND, MonitorSerie::OnAddCommand)
END_EVENT_TABLE()
