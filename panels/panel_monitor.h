#pragma once

#include "../utils/types.h"

#include <wx/event.h>
#include <wx/panel.h>
#include <wx/richtext/richtextctrl.h>
#include <wx/splitter.h>
#include <wx/stattext.h>
#include <wx/wx.h>

class PanelMonitor : public wxPanel {
  public:
    PanelMonitor(wxWindow *parent, wxWindowID id = wxID_ANY,
                 const wxPoint &pos = wxDefaultPosition,
                 const wxSize &size = wxDefaultSize,
                 long style = wxTAB_TRAVERSAL,
                 const wxString &name = wxPanelNameStr);

    void OnSend(wxCommandEvent &event);
    void OnAdd(wxCommandEvent &event);

    void AppendText(const SerialData &data);
    void SetAutoscroll(bool value);
    bool GetAutoscroll() const;

    void BindEvents();
    void UpdateControlStates(wxCommandEvent &event);

  private:
    wxRichTextCtrl *m_richtxt_ctrl;
    bool m_output_is_tx{};
    bool m_autoscroll{};

    wxTextCtrl *m_txt_command{};
    wxButton *m_btn_add{};
    wxButton *m_btn_send{};
};
