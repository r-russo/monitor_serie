#pragma once

#include "../controls/control_command.h"

#include <vector>
#include <wx/event.h>
#include <wx/splitter.h>
#include <wx/wx.h>

class PanelComandos : public wxScrolledWindow {
  public:
    PanelComandos(wxWindow *parent, wxWindowID id = -1,
                  const wxPoint &pos = wxDefaultPosition,
                  const wxSize &size = wxDefaultSize, long style = wxVSCROLL,
                  const wxString &name = "commands");

  private:
    DECLARE_EVENT_TABLE()

    void OnAddCommand(wxCommandEvent &event);
    void OnDeleteCommand(wxCommandEvent &event);

    std::vector<CtrlCommand *> m_commands{};
};
