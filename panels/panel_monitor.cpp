#include "panel_monitor.h"
#include "../utils/events.h"
#include "../utils/types.h"
#include "../utils/utils.h"

#include <iomanip>
#include <sstream>
#include <string>
#include <wx/event.h>
#include <wx/gdicmn.h>
#include <wx/generic/panelg.h>
#include <wx/gtk/colour.h>
#include <wx/gtk/textctrl.h>
#include <wx/gtk/textentry.h>
#include <wx/log.h>
#include <wx/richtext/richtextctrl.h>
#include <wx/scrolwin.h>
#include <wx/string.h>
#include <wx/stringimpl.h>
#include <wx/textctrl.h>

// TODO: buffer con una cantidad máxima de líneas guardadas

PanelMonitor::PanelMonitor(wxWindow *parent, wxWindowID id, const wxPoint &pos,
                           const wxSize &size, long style, const wxString &name)
    : wxPanel(parent, id, pos, size, style, name) {
    auto *sizer_bottom = new wxBoxSizer(wxHORIZONTAL);

    m_txt_command =
        new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition,
                       wxDefaultSize, wxTE_PROCESS_ENTER);
    m_btn_add = new wxButton(this, wxID_ANY, "+");
    m_btn_send = new wxButton(this, wxID_ANY, ">");

    sizer_bottom->Add(m_txt_command, 1, wxEXPAND);
    sizer_bottom->Add(m_btn_add);
    sizer_bottom->Add(m_btn_send);

    auto *sizer = new wxBoxSizer(wxVERTICAL);

    m_richtxt_ctrl = new wxRichTextCtrl(
        this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize,
        wxRE_READONLY | wxRE_MULTILINE | wxVSCROLL | wxHSCROLL | wxBORDER_NONE |
            wxWANTS_CHARS);

    sizer->Add(m_richtxt_ctrl, 1, wxEXPAND);
    sizer->Add(sizer_bottom, 0, wxEXPAND);

    this->SetSizer(sizer);

    BindEvents();

    wxCommandEvent event{};
    UpdateControlStates(event);
}

void PanelMonitor::OnSend(__attribute__((unused)) wxCommandEvent &event) {
    wxString command = m_txt_command->GetLineText(0);
    m_txt_command->Clear();

    wxCommandEvent out_event(MONITOR_SEND_COMMAND);
    out_event.SetString(command);
    wxPostEvent(m_parent, out_event);
}

void PanelMonitor::OnAdd(__attribute__((unused)) wxCommandEvent &event) {
    wxString command = m_txt_command->GetLineText(0);

    wxCommandEvent out_event(MONITOR_ADD_COMMAND);
    out_event.SetString(command);
    wxPostEvent(m_parent, out_event);
}

void PanelMonitor::AppendText(const SerialData &data) {
    auto default_color = m_richtxt_ctrl->GetForegroundColour();
    m_richtxt_ctrl->SetCaretPosition(m_richtxt_ctrl->GetLastPosition());
    m_richtxt_ctrl->SelectNone();
    std::string utc_time = utc_str_from_timestamp(data.timestamp);
    if (data.tx) {
        m_richtxt_ctrl->BeginTextColour(wxColour(0, 255, 0));
        m_richtxt_ctrl->WriteText(wxT("tx ["));
    } else {
        m_richtxt_ctrl->BeginTextColour(wxColour(255, 0, 0));
        m_richtxt_ctrl->WriteText(wxT("rx ["));
    }
    m_richtxt_ctrl->WriteText(utc_time);
    m_richtxt_ctrl->WriteText(wxT("] -> "));
    m_richtxt_ctrl->EndTextColour();

    std::stringstream new_line_stream{};
    for (size_t i = 0; i < data.line.length(); i++) {
        if (std::isprint(data.line[i])) {
            new_line_stream << data.line[i];
        } else {
            // TODO: reemplazar con color desde el richtextctrl
            new_line_stream << "[";
            new_line_stream << std::setfill('0') << std::setw(2) << std::hex
                            << static_cast<int>(data.line[i]);
            new_line_stream << "]";
        }
    }
    wxLogDebug("%s", new_line_stream.str().c_str());
    m_richtxt_ctrl->BeginTextColour(default_color);
    m_richtxt_ctrl->WriteText(new_line_stream.str());
    m_richtxt_ctrl->EndTextColour();

    if (m_autoscroll) {
        m_richtxt_ctrl->ShowPosition(m_richtxt_ctrl->GetLastPosition());
    }
}

void PanelMonitor::SetAutoscroll(bool value) { m_autoscroll = value; }
bool PanelMonitor::GetAutoscroll() const { return m_autoscroll; }

void PanelMonitor::BindEvents() {
    m_btn_add->Bind(wxEVT_BUTTON, &PanelMonitor::OnAdd, this);
    m_btn_send->Bind(wxEVT_BUTTON, &PanelMonitor::OnSend, this);
    m_txt_command->Bind(wxEVT_TEXT_ENTER, &PanelMonitor::OnSend, this);
    m_txt_command->Bind(wxEVT_TEXT, &PanelMonitor::UpdateControlStates, this);
}

void PanelMonitor::UpdateControlStates(__attribute__((unused))
                                       wxCommandEvent &event) {
    m_btn_add->Enable(!m_txt_command->IsEmpty());
    m_btn_send->Enable(!m_txt_command->IsEmpty());
}

wxDEFINE_EVENT(MONITOR_SEND_COMMAND, wxCommandEvent);
wxDEFINE_EVENT(MONITOR_ADD_COMMAND, wxCommandEvent);
