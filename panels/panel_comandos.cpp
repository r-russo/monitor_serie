#include "panel_comandos.h"
#include "../utils/events.h"

#include <algorithm>
#include <wx/event.h>
#include <wx/log.h>
#include <wx/msgdlg.h>
#include <wx/scrolwin.h>
#include <wx/sizer.h>

PanelComandos::PanelComandos(wxWindow *parent, wxWindowID id,
                             const wxPoint &pos, const wxSize &size, long style,
                             const wxString &name)
    : wxScrolledWindow(parent, id, pos, size, style, name) {
    auto *sizer = new wxBoxSizer(wxVERTICAL);

    this->SetSizer(sizer);
    this->FitInside();
    this->SetScrollRate(5, 5);
}

void PanelComandos::OnAddCommand(wxCommandEvent &event) {
    wxString command = event.GetString();
    auto ctrl_command = new CtrlCommand(this, m_parent, command);

    GetSizer()->Add(ctrl_command, 0, wxEXPAND);
    m_commands.push_back(ctrl_command);
    Layout();
}

void PanelComandos::OnDeleteCommand(wxCommandEvent &event) {
    int id = event.GetId();

    auto it = std::find_if(m_commands.begin(), m_commands.end(),
                           [id](CtrlCommand *a) { return a->GetId() == id; });

    for (size_t i = 0; i < GetSizer()->GetItemCount(); i++) {
        if (GetSizer()->GetItem(i)->GetId() == id) {
            GetSizer()->Detach(i);
            break;
        }
    }

    (*it)->Destroy();
    m_commands.erase(it);
    Layout();
}

BEGIN_EVENT_TABLE(PanelComandos, wxScrolledWindow)
EVT_COMMAND(wxID_ANY, MONITOR_ADD_COMMAND, PanelComandos::OnAddCommand)
EVT_COMMAND(wxID_ANY, COMANDOS_DELETE_COMMAND_CONTROL,
            PanelComandos::OnDeleteCommand)
END_EVENT_TABLE()
