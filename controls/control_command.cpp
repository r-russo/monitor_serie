#include "control_command.h"
#include "../utils/events.h"
#include "../utils/globals.h"

#include <wx/event.h>

CtrlCommand::CtrlCommand(wxWindow *parent, wxWindow *event_handler,
                         wxString command, wxWindowID id, const wxPoint &pos,
                         const wxSize &size, long style, const wxString &name)
    : wxWindow(parent, id, pos, size, style, name) {
    m_command_str = command;
    m_event_handler = event_handler;

    auto label = new wxStaticText(this, wxID_ANY, command);
    // TODO: usar iconos
    m_btn_send = new wxButton(this, wxID_ANY, "->");
    m_btn_delete = new wxButton(this, wxID_ANY, "X");

    auto sizer = new wxBoxSizer(wxHORIZONTAL);

    sizer->Add(label, 1, wxEXPAND | wxALL, PADDING);
    sizer->Add(m_btn_send, 0, wxALL, PADDING);
    sizer->Add(m_btn_delete, 0, wxALL, PADDING);

    SetSizer(sizer);

    BindEvents();
}

void CtrlCommand::OnSend(__attribute__((unused)) wxCommandEvent &event) {
    wxCommandEvent out_event(MONITOR_SEND_COMMAND);
    out_event.SetString(m_command_str);
    wxPostEvent(m_event_handler, out_event);
}

void CtrlCommand::OnDelete(__attribute__((unused)) wxCommandEvent &event) {
    wxCommandEvent out_event(COMANDOS_DELETE_COMMAND_CONTROL);
    out_event.SetId(GetId());
    wxPostEvent(m_parent, out_event);
}

void CtrlCommand::BindEvents() {
    m_btn_send->Bind(wxEVT_BUTTON, &CtrlCommand::OnSend, this);
    m_btn_delete->Bind(wxEVT_BUTTON, &CtrlCommand::OnDelete, this);
}

wxDEFINE_EVENT(COMANDOS_DELETE_COMMAND_CONTROL, wxCommandEvent);
