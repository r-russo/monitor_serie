#pragma once

#include <wx/wx.h>

class CtrlCommand : public wxWindow {
  public:
    CtrlCommand(wxWindow *parent, wxWindow *event_handler, wxString command,
                wxWindowID id = -1, const wxPoint &pos = wxDefaultPosition,
                const wxSize &size = wxDefaultSize, long style = 0,
                const wxString &name = "commandctrl");

  private:
    void OnSend(wxCommandEvent &event);
    void OnDelete(wxCommandEvent &event);
    void BindEvents();

    wxString m_command_str{};
    wxButton *m_btn_send{};
    wxButton *m_btn_delete{};
    wxWindow *m_event_handler{};
};
