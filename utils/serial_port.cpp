#include "serial_port.h"
#include <errno.h>
#include <filesystem>
#include <memory>
#include <optional>
#include <string>
#include <unordered_map>
#include <wx/log.h>

static std::optional<int> convert_baudrate(int baudrate) {
    switch (baudrate) {
    case 50:
        return B50;
    case 75:
        return B75;
    case 110:
        return B110;
    case 134:
        return B134;
    case 150:
        return B150;
    case 200:
        return B200;
    case 300:
        return B300;
    case 600:
        return B600;
    case 1200:
        return B1200;
    case 1800:
        return B1800;
    case 2400:
        return B2400;
    case 4800:
        return B4800;
    case 9600:
        return B9600;
    case 19200:
        return B19200;
    case 38400:
        return B38400;
    case 57600:
        return B57600;
    case 115200:
        return B115200;
    case 230400:
        return B230400;
    case 460800:
        return B460800;
    case 500000:
        return B500000;
    case 576000:
        return B576000;
    case 921600:
        return B921600;
    case 1000000:
        return B1000000;
    case 1152000:
        return B1152000;
    case 1500000:
        return B1500000;
    case 2000000:
        return B2000000;
    case 2500000:
        return B2500000;
    case 3000000:
        return B3000000;
    case 3500000:
        return B3500000;
    case 4000000:
        return B4000000;
    }
    return {};
}

SerialPort::SerialPort() {}

SerialPort::~SerialPort() { closePort(); }

void SerialPort::updateAvailablePorts() {
    std::filesystem::path path = "/dev/serial/by-id/";
    available_ports.clear();
    if (!std::filesystem::exists(path)) {
        return;
    }

    for (const auto &entry : std::filesystem::directory_iterator(path)) {
        std::string name{};
        if (std::filesystem::is_symlink(entry)) {
            name = std::filesystem::read_symlink(entry).stem().string();
        } else {
            name = entry.path().stem().string();
        }
        available_ports[name] = entry.path().string();
    }
}

const std::unordered_map<std::string, std::string> &
SerialPort::getAvailablePorts() {
    return available_ports;
}

const std::vector<int> &SerialPort::getAvailableBaudRates() {
    return available_baudrates;
}

void SerialPort::setPort(const std::string &port) { m_port = port; }

void SerialPort::setBaudRate(int baudrate) {
    m_baudrate = convert_baudrate(baudrate).value_or(0);
}

bool SerialPort::openPort(int timeout_ds) {
    if (m_open) {
        wxLogDebug("Port not closed. Forcing...");
        closePort();
    }
    if (m_port == "") {
        wxLogDebug("Port not set");
        return false;
    }

    if (m_baudrate == 0) {
        wxLogDebug("Baud rate not set");
        return false;
    }

    if (available_ports.count(m_port) > 0) {
        m_serialport = open(available_ports.at(m_port).c_str(), O_RDWR);
    } else {
        m_serialport = open(m_port.c_str(), O_RDWR);
    }

    if (m_serialport < 0) {
        wxLogError("Error %i from open: %s", errno, strerror(errno));
        return false;
    }

    // FIXME: llevar a una función de setup
    // ref:
    // https://tldp.org/HOWTO/Serial-Programming-HOWTO/x115.html
    static struct termios tty;

    // if (tcgetattr(m_serialport, &tty) != 0) {
    //     wxLogError("Error %i from tcgetattr: %s\n", errno, strerror(errno));
    //     return false;
    // }

    bzero(&tty, sizeof(tty));
    tty.c_cflag = m_baudrate | CS8 | CLOCAL | CREAD;
    tty.c_iflag = IGNPAR;
    tty.c_oflag = 0;
    tty.c_cc[VTIME] = timeout_ds;
    tty.c_cc[VMIN] = 0;

    tcflush(m_serialport, TCIFLUSH);
    tcsetattr(m_serialport, TCSANOW, &tty);

    m_open = true;

    return true;
}

void SerialPort::closePort() {
    close(m_serialport);
    m_open = false;
}

std::string SerialPort::getPort() const { return m_port; }

int SerialPort::getBaudRate() const { return m_baudrate; }

bool SerialPort::isOpen() const { return m_open; }

bool SerialPort::readBytes(std::string &buffer, size_t n_bytes) {
    if (!m_open) {
        wxLogDebug("Port not open");
        return false;
    }

    auto buffer_ptr = std::unique_ptr<char>(new char[n_bytes]);
    int res = read(m_serialport, buffer_ptr.get(), n_bytes);
    buffer_ptr.get()[res] = '\0';
    buffer = std::string(buffer_ptr.get());

    return true;
}

bool SerialPort::writeBuffer(const std::string &buffer) {
    if (!m_open) {
        wxLogDebug("Port not open");
        return false;
    }

    // FIXME: escribe solo 8
    int ret = write(m_serialport, buffer.c_str(), buffer.size());

    if (ret == -1) {
        wxLogDebug("Error %i from write: %s", errno, strerror(errno));
        return false;
    }
    return true;
}
