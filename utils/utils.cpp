#include "utils.h"

std::string utc_str_from_timestamp(std::time_t time) {
    char timeString[std::size("yyyy-mm-ddThh:mm:ssZ")];
    std::strftime(std::data(timeString), std::size(timeString), "%FT%TZ",
                  std::gmtime(&time));
    return std::string(timeString);
}
