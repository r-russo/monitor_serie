#pragma once

#include <ctime>
#include <string>

typedef struct SerialData {
    std::time_t timestamp{};
    std::string line{};
    bool tx{};
} SerialData;