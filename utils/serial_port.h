#pragma once

#include <fcntl.h>
#include <string>
#include <termios.h>
#include <unistd.h>
#include <wx/log.h>

static std::unordered_map<std::string, std::string> available_ports{};
static std::vector<int> available_baudrates{
    50,      75,      110,     134,     150,     200,    300,     600,
    1200,    1800,    2400,    4800,    9600,    19200,  38400,   57600,
    115200,  230400,  460800,  500000,  576000,  921600, 1000000, 1152000,
    1500000, 2000000, 2500000, 3000000, 3500000, 4000000};

class SerialPort {
  public:
    SerialPort();
    ~SerialPort();

    static void updateAvailablePorts();
    static const std::unordered_map<std::string, std::string> &
    getAvailablePorts();
    static const std::vector<int> &getAvailableBaudRates();

    void setPort(const std::string &port);
    void setBaudRate(int baudrate);

    bool openPort(int timeout_ds = 1);
    void closePort();

    std::string getPort() const;
    int getBaudRate() const;
    bool isOpen() const;

    bool readBytes(std::string &buffer, size_t n_bytes = 128);
    bool writeBuffer(const std::string &buffer);

  private:
    std::string m_port{};
    int m_baudrate{};
    int m_serialport{};
    bool m_open{};
};