#pragma once

#include <ctime>
#include <string>

std::string utc_str_from_timestamp(std::time_t time);