#pragma once

#include "panels/panel_comandos.h"
#include "panels/panel_monitor.h"
#include "utils/serial_port.h"

#include <deque>
#include <wx/event.h>
#include <wx/menu.h>
#include <wx/splitter.h>
#include <wx/timer.h>
#include <wx/wx.h>

enum {
    ID_COMMANDS = wxID_HIGHEST + 1,
    ID_NEW,
    ID_RUN,
    ID_STOP,
    ID_CONFIG,
    ID_OPEN_COMMANDS,
    ID_SAVE_COMMANDS,
    ID_SAVE_LOG,
    ID_MONITOR,
    ID_TOGGLE_MONITOR_SCROLL
};

class MonitorSerie : public wxFrame {
  public:
    MonitorSerie(const wxString &title);

    void OnSetup(wxCommandEvent &event);
    void OnRun(wxCommandEvent &event);
    void OnStop(wxCommandEvent &event);
    void OnToggleMonitorScroll(wxCommandEvent &event);
    void OnTimer(wxTimerEvent &event);
    void OnSendCommand(wxCommandEvent &event);
    void OnAddCommand(wxCommandEvent &event);

  private:
    DECLARE_EVENT_TABLE()
    void GenerateMenubar();
    void InitControlStates();
    void BindEvents();
    void UpdateControlStates();

    SerialPort m_serial_port{};
    wxTimer m_timer{};
    std::deque<SerialData> m_serial_buffer{};

    PanelMonitor *m_panel_monitor;
    PanelComandos *m_panel_comandos;
};

class App : public wxApp {
  public:
    virtual bool OnInit();
};
