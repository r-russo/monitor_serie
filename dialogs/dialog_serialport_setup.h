#pragma once

#include <wx/combobox.h>
#include <wx/dialog.h>
#include <wx/event.h>
#include <wx/gdicmn.h>
#include <wx/timer.h>
#include <wx/toplevel.h>
#include <wx/wx.h>

class DialogSerialPortSetup : public wxDialog {
  public:
    DialogSerialPortSetup(
        wxWindow *parent, wxWindowID id = wxID_ANY,
        const wxString &caption = wxT("Configuración de puerto serie"),
        const wxPoint &pos = wxDefaultPosition,
        const wxSize &size = wxDefaultSize,
        long style = wxCAPTION | wxSYSTEM_MENU | wxCLOSE_BOX);

    bool TransferDataFromWindow();

    std::string GetPort() const;
    int GetBaudRate() const;

  private:
    void OnTimerDevices(wxTimerEvent &event);

    void BindEvents();
    void updateAvailablePorts();
    void updateAvailableBaudRates();

    std::string m_port{};
    int m_baudrate{};

    wxChoice *m_ch_port{};
    wxChoice *m_ch_baudrate{};
    wxTimer m_timer_devices{};
};
