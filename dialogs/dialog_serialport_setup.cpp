#include "dialog_serialport_setup.h"
#include "../utils/serial_port.h"

#include <string>
#include <wx/gtk/button.h>
#include <wx/gtk/choice.h>
#include <wx/gtk/combobox.h>
#include <wx/gtk/dialog.h>
#include <wx/sizer.h>
#include <wx/statline.h>
#include <wx/string.h>
#include <wx/timer.h>

DialogSerialPortSetup::DialogSerialPortSetup(wxWindow *parent, wxWindowID id,
                                             const wxString &caption,
                                             const wxPoint &pos,
                                             const wxSize &size, long style)
    : wxDialog(parent, id, caption, pos, size, style) {

    wxBoxSizer *vertical_sizer = new wxBoxSizer(wxVERTICAL);
    wxGridSizer *grid_sizer = new wxGridSizer(2, 3, 3);
    wxBoxSizer *button_sizer = new wxBoxSizer(wxHORIZONTAL);

    auto *lbl_port = new wxStaticText(this, wxID_ANY, wxT("Puerto"));
    auto *lbl_baudrate = new wxStaticText(this, wxID_ANY, wxT("Baud Rate"));
    m_ch_port = new wxChoice(this, wxID_ANY);
    m_ch_baudrate = new wxChoice(this, wxID_ANY);

    auto *btn_ok = new wxButton(this, wxID_OK, wxT("Ok"));
    auto *btn_cancel = new wxButton(this, wxID_CANCEL, wxT("Cancelar"));

    grid_sizer->Add(lbl_port, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
    grid_sizer->Add(m_ch_port, 0, wxALL, 5);
    grid_sizer->Add(lbl_baudrate, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
    grid_sizer->Add(m_ch_baudrate, 0, wxALL, 5);

    button_sizer->Add(btn_ok, 0, wxALL, 5);
    button_sizer->Add(btn_cancel, 0, wxALL, 5);

    vertical_sizer->Add(grid_sizer, 0, wxALL, 5);
    vertical_sizer->Add(button_sizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);

    this->SetSizer(vertical_sizer);
    vertical_sizer->Fit(this);
    vertical_sizer->SetSizeHints(this);

    BindEvents();
    updateAvailablePorts();
    updateAvailableBaudRates();

    m_timer_devices.Start(1000);
}

bool DialogSerialPortSetup::TransferDataFromWindow() {
    m_port = m_ch_port->GetStringSelection();
    m_baudrate = std::stoi(m_ch_baudrate->GetStringSelection().ToStdString());

    return true;
}

std::string DialogSerialPortSetup::GetPort() const { return m_port; }

int DialogSerialPortSetup::GetBaudRate() const { return m_baudrate; }

void DialogSerialPortSetup::OnTimerDevices(__attribute__((unused))
                                           wxTimerEvent &event) {
    updateAvailablePorts();
}

void DialogSerialPortSetup::BindEvents() {
    m_timer_devices.Bind(wxEVT_TIMER, &DialogSerialPortSetup::OnTimerDevices,
                         this);
}

void DialogSerialPortSetup::updateAvailablePorts() {
    SerialPort::updateAvailablePorts();
    auto serial_devices = SerialPort::getAvailablePorts();
    wxString selection = m_ch_port->GetStringSelection();
    m_ch_port->Clear();
    for (const auto &[device, _] : serial_devices) {
        m_ch_port->Append(device);
    }
    int new_selection_pos = m_ch_port->FindString(selection);
    if (new_selection_pos != wxNOT_FOUND) {
        m_ch_port->SetSelection(new_selection_pos);
    } else {
        m_ch_port->SetSelection(0);
    }
}

void DialogSerialPortSetup::updateAvailableBaudRates() {
    std::vector<int> baudrates = SerialPort::getAvailableBaudRates();
    wxString selection = m_ch_baudrate->GetStringSelection();
    m_ch_baudrate->Clear();
    for (const auto &baudrate : baudrates) {
        m_ch_baudrate->Append(std::to_string(baudrate));
    }
    int new_selection_pos = m_ch_baudrate->FindString(selection);
    if (new_selection_pos != wxNOT_FOUND) {
        m_ch_baudrate->SetSelection(new_selection_pos);
    } else {
        m_ch_baudrate->SetSelection(m_ch_baudrate->FindString("115200"));
    }
}
